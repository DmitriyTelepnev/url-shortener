# URL Shortener

## Requirements

* docker: ^ 17.12.1(maybe it run on 1.13.1, i had a successful experience)

* docker-compose: ^ 1.17.1

* access to to the internet (for clone project and pull docker containers)

* free 5432 port on machine (for postgresql container, otherwise it would have to be configured pg_hba.conf)

* git

## Getting started

### Run it

Clone git repo
```bash
git clone git@bitbucket.org:DmitriyTelepnev/url-shortener.git
```

For start app simply run bash in project dir
```bash
sudo ./start.sh
```
And wait until in console print "Successfully build"
Follow the link ```http://localhost:64``` in the browser

For stop app
```bash
sudo ./stop.sh
```

Clear containers
```bash
sudo ./stop.sh kill
```

For delete images you must yourself check docker images and run
```bash
sudo docker rmi <imageId> 
``` 

## Advanced

Application consist of 4 docker containers:

* **nginx/unit** for backend API (reserves 62 port)
* **nginx** for static front (reserves 64 port)
* **postgresql** for store data (reserves 5432 port)
* **composer** for install dependencies

**nginx/unit** depends on **postgresql** and **composer**
