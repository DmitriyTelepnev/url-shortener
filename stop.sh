#!/bin/bash

containers=$(docker ps -a | grep -E 'url[-_]*shortener' | grep -o -E '^[a-zA-Z0-9]+')

for container in $containers
do
    if [[ $1 = 'kill' ]]
    then
        docker rm -f $container
    else
        docker stop $container
    fi

done;
