#!/bin/bash

# Build and start containers
docker-compose up --build -d

container=$(docker ps | grep -E 'url[-_]*shortener[-_]*web[-_]*1' | grep -o -E '^[a-zA-Z0-9]+')

# Configure Nginx-unit
docker exec -it $container curl -X PUT -d @/unit/conf.json --unix-socket /var/run/control.unit.sock http://localhost/

composerContainer=$(docker ps | grep 'composer' | grep -o -E '^[a-zA-Z0-9]+')

if [[ -n $composerContainer ]]
then
    docker attach $composerContainer;
fi

echo "Successfully build";