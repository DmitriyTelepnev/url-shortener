<?php
require_once 'vendor/autoload.php';

use telepnevde\urlShortener\core\Application;

Application::instanitate()
    ->dispatchRequest($_SERVER['REQUEST_METHOD'], $_SERVER['REQUEST_URI']);