<?php

return [
    'db' => [
        'dsn' => 'pgsql:dbname=test;host=postgres;port=5432',
        'username' => 'postgres',
        'password' => 'mysqld'
    ],
    'routing' => [
        'routes' => [
            '^\/urls\/$' => [
                'class' => 'telepnevde\urlShortener\controllers\Url',
                'requestMethod' => 'POST',
                'action' => 'convert'
            ],
            '^\/[A-Za-z0-9]+$' => [
                'class' => 'telepnevde\urlShortener\controllers\Url',
                'requestMethod' => 'GET',
                'action' => 'deconvert'
            ]
        ]
    ]
];
