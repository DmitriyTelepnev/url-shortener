const config = {
    backendUrl: 'http://localhost:62'
};

/**
 * render
 * @param object result
 */
const renderResult = result => {
    const url = config.backendUrl + '/' + result.shortUrl;
    document
        .getElementById('result')
        .innerHTML = `<a href="${url}">${url}</a>`;
}

/**
 * Fetch short url
 * @param {type} url
 */
const shortUrl = url => {
    const form = new FormData();
    form.set('url', url);
    
    fetch(config.backendUrl + '/urls/', {
            method: "POST",
            body: form
        })
        .then(response => {
            response.json()
                .then(renderResult);
        })
        .catch(console.warn);
}
