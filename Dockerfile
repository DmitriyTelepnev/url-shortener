FROM nginx/unit:1.3-full

RUN set -ex \
    && echo "deb http://ftp.de.debian.org/debian stretch main" >> /etc/apt/sources.list \
    && apt-get update \
    && apt-get install -y php-pgsql

WORKDIR /var/www
CMD ["unitd", "--no-daemon"]