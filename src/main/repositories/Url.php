<?php

namespace telepnevde\urlShortener\repositories;

use \telepnevde\urlShortener\core\Application;

/**
 * Url Repository
 */
class Url {
    
    /**
     * Save url
     * @param type $url
     */
    public function save($url)
    {
        /**
         * In case when url exists in table "RETURNING id" not return id))
         * Or need using Postgres CTE
         */
        $this->insert($url);
        return $this->getId($url);
    }
    
    /**
     * Insert record
     * @param type $url
     * @return type
     */
    private function insert($url)
    {
        $statement = Application::app()
                ->db
                ->prepare('
                        INSERT INTO urls (url)
                        VALUES (:url)
                        ON CONFLICT DO NOTHING
                    ');
        return $statement->execute([':url' => $url]);
    }
    
    /**
     * Get id
     * @param type $url
     * @return type
     */
    private function getId($url)
    {
        $statement = Application::app()
                ->db
                ->prepare('SELECT id FROM urls WHERE url = :url');
        $statement->execute([':url' => $url]);
        return $statement->fetchColumn();
    }
    
    /**
     * Find url by id
     * @param type $id
     */
    public function getById($id)
    {
        $statement = Application::app()
                ->db
                ->prepare('SELECT url FROM urls WHERE id = :id');
        $statement->execute([':id' => $id]);
        return $statement->fetchColumn();
    }
    
}
