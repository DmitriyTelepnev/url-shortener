<?php

namespace telepnevde\urlShortener\validators;

/**
 * Url Validator
 */
class UrlValidator
{

    /**
     * 
     * @param string $url
     * @return bool
     */
    public static function validate(string $url) : bool
    {
        return (bool) filter_var($url, FILTER_VALIDATE_URL);
    }
    
}
