<?php

namespace telepnevde\urlShortener\controllers;

use telepnevde\urlShortener\validators\UrlValidator;
use telepnevde\urlShortener\services\IdConverter;
use telepnevde\urlShortener\repositories\Url as UrlRepo;
use telepnevde\urlShortener\core\exceptions\NotFoundException;
use \telepnevde\urlShortener\core\controllers\RestController;

/**
 * Controller
 */
class Url extends RestController
{

    /**
     * Validator
     * @var UrlValidator 
     */
    protected $validator;
    
    /**
     * Shortener service
     * @var IdConverter
     */
    protected $shortenerService;
    
    /**
     * Repository
     * @var UrlRepo
     */
    protected $repository;

    /**
     * ToDo DI
     */
    public function __construct()
    {
        $this->validator = new UrlValidator;
        $this->shortenerService = new IdConverter;
        $this->repository = new UrlRepo();
    }
    
    /**
     * Create short url
     * @return array
     * @throws NotFoundException
     */
    public function convert() : array
    {
        // For RESTfull app i must read stdin... But not today
        $url = strip_tags(trim($_POST['url'] ?? ''));
        if(!$url || !$this->validator->validate($url)) {
            throw new NotFoundException();
        }
        
        return [
            'shortUrl' => $this->shortenerService->convert(
                $this->repository->save($url)
            )
        ];
    }
    
    /**
     * Get source url by short url
     * @return type
     * @throws NotFoundException
     */
    public function deconvert()
    {
        $url = $this->repository->getById(
            $this->shortenerService->deconvert($_SERVER['REQUEST_URI'])
        );
        
        if(!$url) {
            throw new NotFoundException();
        }
        
        header('Location: ' . $url);
    }
    
}
