<?php

namespace telepnevde\urlShortener\services;

/**
 * int converter
 */
class IdConverter
{
    
    const SOURCE_SCALE_OF_NOTATION = 10;
    const DESTINATION_SCALE_OF_NOTATION = 36;
    
    /**
     * Encode id
     * @param int $id
     * @return string
     */
    public static function convert(int $id) : string
    {
        return base_convert(
                $id,
                self::SOURCE_SCALE_OF_NOTATION,
                self::DESTINATION_SCALE_OF_NOTATION
        );
    }
    
    /**
     * Decode id
     * @param string $hash
     * @return int
     */
    public static function deconvert(string $hash) : int
    {
        return base_convert(
                $hash,
                self::DESTINATION_SCALE_OF_NOTATION,
                self::SOURCE_SCALE_OF_NOTATION
        );
    }
    
}
