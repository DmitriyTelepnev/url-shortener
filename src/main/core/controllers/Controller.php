<?php

namespace telepnevde\urlShortener\core\controllers;

/**
 * Controller interface
 */
interface Controller {
    
    /**
     * render result of action
     * @param type $result
     */
    public function render($result);
    
}
