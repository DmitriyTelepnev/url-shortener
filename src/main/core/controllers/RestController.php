<?php

namespace telepnevde\urlShortener\core\controllers;

/**
 * REST Controller;
 */
class RestController implements Controller
{

    /**
     * @inheritdoc
     * @param type $result
     */
    public function render($result)
    {
        header('Content-type: application/json');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: POST, GET');
        echo json_encode($result);
    }
    
}
