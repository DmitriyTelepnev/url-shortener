<?php

namespace telepnevde\urlShortener\core;

use telepnevde\urlShortener\core\exceptions\{
    NotFoundException,
    ConfigurationException
};
use telepnevde\urlShortener\core\controllers\Controller;

/**
 * Class violates the SOLID principle because of my task to do MVP
 * But this class i can refactor without destroying full application
 */
class Application
{

    /**
     * application instance
     * @var self
     */
    protected static $app;

    /**
     * config collection
     * @var []
     */
    protected $config;

    /**
     * Db Connection
     * @var \PDO
     */
    public $db;

    private function __construct($config)
    {
        $this->config = $config;
        $this->db = $this->instantiateDbConn(
            $config['db']['dsn'],
            $config['db']['username'],
            $config['db']['password']
        );
    }
    
    /**
     * Instantiate Db connection
     * @param type $dsn
     * @param type $username
     * @param type $password
     * @return \PDO
     */
    private function instantiateDbConn($dsn, $username, $password)
    {
        return new \PDO($dsn, $username, $password);
    }
    
    /**
     * 
     * @return self
     */
    public static function app()
    {
        return self::$app;
    }
    
    /**
     * Instantitate application
     * @return self
     */
    public static function instanitate()
    {
        return self::$app = new static(
            self::loadConfig()
        );
    }
    
    /**
     * Load application config
     * @return []
     */
    private static function loadConfig()
    {
        return require_once __DIR__ . '/../../../config/main.php';
    }

    /**
     * Dispatch request
     * @param string $method
     * @param string $uri
     */
    public function dispatchRequest(string $method, string $uri)
    {
        foreach($this->config['routing']['routes'] as $pattern => $route) {
            if(preg_match("/$pattern/", $uri) && $method === $route['requestMethod']) {
                $controller = $this->instantiateController($route['class']);
                $action = $route['action'];
                
                if(method_exists($controller, $action)) {
                    return $controller->render(
                        $controller->$action()
                    );
                }
            }
        }
        
        throw new NotFoundException();
    }
    
    /**
     * 
     * @param type $controllerClass
     * @return Controller
     * @throws ConfigurationException
     */
    private function instantiateController($controllerClass)
    {
        $controller = new $controllerClass;
        
        if(!$controller instanceof Controller) {
            throw new ConfigurationException();
        }
        
        return $controller;
    }
    
}
