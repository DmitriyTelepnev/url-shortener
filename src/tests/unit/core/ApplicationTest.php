<?php
namespace tests\unit\core;

use telepnevde\urlShortener\core\Application;

class ApplicationTest extends \Codeception\Test\Unit
{
    /**
     * @var \tests\UnitTester
     */
    protected $tester;
    
    public function testDispatchRequest()
    {
        foreach($this->dispatchRequestDataProvider() as $testName => $data) {
            $application = $this->make(Application::class, ['config' => [
                'routing' => [
                    'routes' => $data['routes']
                ]
            ]]);

            $this->assertEquals(
                $data['expectedResult'],
                $application->dispatchRequest($data['method'], $data['uri']),
                "in case '$testName'"
            );
        }
    }
    
    public function dispatchRequestDataProvider() : array
    {
        return [
            'test GET /' => [
                'routes' => [
                    '^\/$' => [
                        'class' => new class implements \telepnevde\urlShortener\core\controllers\Controller {
                            public function test() {
                                return 'asd';
                            }
                            
                            public function render($result) {
                                return $result;
                            }
                        },
                        'requestMethod' => 'GET',
                        'action' => 'test'
                    ]
                ],
                'uri' => '/',
                'method' => 'GET',
                'expectedResult' => 'asd'
            ],
            'test GET /asdasd' => [
                'routes' => [
                    '^\/[A-Za-z0-9]+$' => [
                        'class' => new class implements \telepnevde\urlShortener\core\controllers\Controller {
                            public function test() {
                                return 'asd';
                            }
                            
                            public function render($result) {
                                return $result;
                            }
                        },
                        'requestMethod' => 'GET',
                        'action' => 'test'
                    ]
                ],
                'uri' => '/asdasd',
                'method' => 'GET',
                'expectedResult' => 'asd'
            ],
            'test POST /asdasd' => [
                'routes' => [
                    '^\/[A-Za-z0-9]+$' => [
                        'class' => new class implements \telepnevde\urlShortener\core\controllers\Controller {
                            public function test() {
                                return 'asd';
                            }
                            
                            public function render($result) {
                                return $result;
                            }
                        },
                        'requestMethod' => 'POST',
                        'action' => 'test'
                    ]
                ],
                'uri' => '/asdasd',
                'method' => 'POST',
                'expectedResult' => 'asd'
            ],
        ];
    }
    
    /**
     * @expectedException \telepnevde\urlShortener\core\exceptions\NotFoundException
     */
    public function testRequestNotFound()
    {
        $application = $this->make(Application::class, [
            'config' => [
                'routing' => [
                    'routes' => []
                ]
            ]
        ]);
        $application->dispatchRequest('GET', 'url1');
    }
    
    /**
     * @expectedException \telepnevde\urlShortener\core\exceptions\NotFoundException
     */
    public function testRequestNotFoundActionIntoController()
    {
        $application = $this->make(Application::class, [
            'config' => [
                'routing' => [
                    'routes' => [
                        '^\/[A-Za-z0-9]+$' => [
                            'class' => new class {
                                public function test() {
                                    return 'asd';
                                }
                            },
                            'requestMethod' => 'GET',
                            'action' => 'test'
                        ]
                    ]
                ]
            ]
        ]);
        $application->dispatchRequest('GET', 'url1');
    }
    
}