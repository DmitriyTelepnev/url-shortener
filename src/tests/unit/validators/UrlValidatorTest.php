<?php
namespace tests\unit\validators;

use \telepnevde\urlShortener\validators\UrlValidator;

class UrlValidatorTest extends \Codeception\Test\Unit
{
    
    /**
     * Validator
     * @var UrlValidator
     */
    protected $validator;
    
    protected function _before()
    {
        $this->validator = new UrlValidator();
    }

    protected function _after()
    {
        unset($this->validator);
    }
    
    /**
     * @dataProvider validateDataProvider
     */
    public function testValidate($url, $isValid)
    {
        $this->assertEquals(
            $isValid,
            $this->validator->validate($url)
        );
    }
    
    public function validateDataProvider() : array
    {
        return [
            'valid google url' => [
                'url' => 'https://google.com',
                'isValid' => true
            ],
            'valid vk url' => [
                'url' => 'https://vk.com',
                'isValid' => true
            ],
            'invalid empty url' => [
                'url' => '',
                'isValid' => false
            ],
            'invalid number url' => [
                'url' => '123123',
                'isValid' => false
            ]
        ];
    }
    
    /**
     * @expectedException \TypeError
     */
    public function testValidateNullTypeError()
    {
        $this->validator->validate(null);
    }
}