<?php

namespace tests\unit\services;

use telepnevde\urlShortener\services\IdConverter;

class IdConverterTest extends \Codeception\Test\Unit
{
    /**
     * @var \tests\UnitTester
     */
    protected $tester;
    
    /**
     * Id converter
     * @var IdConverter
     */
    protected $idConverter;
    
    protected function _before()
    {
        $this->idConverter = new IdConverter();
    }

    protected function _after()
    {
        unset($this->idConverter);
    }

    /**
     * Convert int to destination scale
     * @dataProvider convertDataProvider
     */
    public function testConvert($id, $length)
    {
        $this->assertLessThan(
            $length,
            strlen($this->idConverter->convert($id))
        );
    }
    
    public function convertDataProvider() : array
    {
        return [
            '100000 - converted string less 7 symbols' => [
                'id' => 100000,
                'length' => 7
            ],
            '1000000 - converted string less 7 symbols' => [
                'id' => 1000000,
                'length' => 7
            ],
            '10000000 - converted string less 7 symbols' => [
                'id' => 10000000,
                'length' => 7
            ],
            '100000000 - converted string less 7 symbols' => [
                'id' => 100000000,
                'length' => 7
            ]
        ];
    }
    
    /**
     * Convert int from destination scale
     * In this test i can assume that result of deconvert converted int equals source int
     */
    public function testDeconvert()
    {
        foreach(range(9999999, 10000023) as $id) {
            $this->assertEquals(
                $id,
                $this->idConverter->deconvert(
                    $this->idConverter->convert($id)
                )
            );
        }
    }
    
    /**
     * @expectedException \TypeError
     */
    public function testNullConvertTypeError()
    {
        $this->idConverter->convert(null);
    }
    
    /**
     * @expectedException \TypeError
     */
    public function testStringConvertTypeError()
    {
        $this->idConverter->convert('asd');
    }
    
    /**
     * @expectedException \TypeError
     */
    public function testNullDeconvertTypeError()
    {
        $this->idConverter->deconvert(null);
    }
    
}